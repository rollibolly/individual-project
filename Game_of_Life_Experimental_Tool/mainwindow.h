#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QPixmap>
#include <QThread>
#include <random>
#include <simulator.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    void init();
    void startSimulation();


    int BOARD_WIDTH = 800;
    int BOARD_HEIGHT = 600;
    int CENTER_X = 300;
    int CENTER_Y = 300;

    QPixmap* pixMap;
    QPainter* painter;
    QColor dotColor;
    QColor background;
    QBrush brush;
    QPen pen;

    Simulator* simulator;
    QThread* thread;

    int dotSize;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::MainWindow *ui;
protected:
    void paintEvent(QPaintEvent *);
public slots:
    // Methods for graphics:
    void drawDot(int x, int y);
    void clearDot(int x, int y);
    void clearScreen();
    void updateCounters();
private slots:
    void on_buttonStart_clicked();
};

#endif // MAINWINDOW_H
