#include "simulator.h"

Simulator::Simulator()
{
    hashBoard = new map<Index, int>;
    heatMap = new map<Index, int>;
    updateList = new map<Index, int>;

    heat = false;
    dimX = 100;
    dimY = 100;

    iterationCount = 0;
    currentCount = 0;
    totalBirth = 0;
    totalDeath = 0;

    stop = false;

    speed = 10;
}

Simulator::~Simulator()
{
    delete hashBoard;
    delete heatMap;
    delete updateList;
}

void Simulator::insertLife(Index i){
    int cVal = 0;
    hashBoard->insert(make_pair(i,0));
    if (heat){
        if (heatMap->find(i) == heatMap->end()){
            heatMap->insert(make_pair(i, 1));
        } else {
            cVal = heatMap->at(i);
            heatMap->insert(make_pair(i, cVal * 10));
        }
    }
    ++currentCount;
    ++totalBirth;
    emit drawDotAt(i.first, i.second);
}

void Simulator::removeLife(Index i){
    hashBoard->erase(i);

    --currentCount;
    ++totalDeath;
    if (!heat){
        emit this->clearDotAt(i.first, i.second);
    }
}

void Simulator::incrementCell(Index i){

    map<Index, int>::iterator it;
    it = updateList->find(i);

    if (it == updateList->end()){
        updateList->insert(make_pair(i, 1));
    } else {
        int value = it-> second;
        ++value;
        updateList->erase(i);
        updateList->insert(make_pair(i, value));
    }
}

void Simulator::updateLists(){
    int i,j;

    map<Index, int>::iterator it;
    for (it = hashBoard->begin(); it != hashBoard->end(); ++it){
        i = it->first.first;    // Index->x
        j = it->first.second;   // Index->y

        if (updateList->find(it->first) == updateList->end()){
            updateList->insert(make_pair(it->first, 0));
        }

        incrementCell(Index(i+1,j+1));
        incrementCell(Index(i+1,j-1));
        incrementCell(Index(i+1,j));
        incrementCell(Index(i-1,j+1));
        incrementCell(Index(i-1,j-1));
        incrementCell(Index(i-1,j));
        incrementCell(Index(i,j+1));
        incrementCell(Index(i,j-1));
    }
}

void Simulator::iterate(){
    updateLists();

    map<Index, int>::iterator it;
    for (it = updateList->begin(); it != updateList->end(); ++it){

        int value = it->second;

        if (hashBoard->find(it->first) == hashBoard->end()){
            if (value == 3){
                insertLife(it->first);
            }
        } else {
            if ((value != 3) && (value != 2)) {
                removeLife(it->first);
            }
        }
    }
    ++iterationCount;
    emit updateCounters();
    updateList->clear();
}

void Simulator::uploadRandom(double precent){
    int count = (dimX * dimY * precent) / 100;
    int i,j;

    while (count > 0){
        i = dimX / 2 - random() % dimX;
        j = dimY / 2 - random() % dimY;

        Index possible(i, j);
        if (hashBoard->find(possible) == hashBoard->end()){
            insertLife(possible);
            --count;
        }
    }
}

void Simulator::reloade(){
    int x, y;

    map<Index, int>::iterator it;
    for (it = hashBoard->begin(); it != hashBoard->end(); ++it){
        x = it->first.first;    // Index->x
        y = it->first.second;   // Index->y
        emit drawDotAt(x, y);
    }
    emit doRepaint();
}
/*
    Entry point of Simulator
*/
void Simulator::process(){

    while(!stop){
        iterate();
        emit this->doRepaint();
        QThread::msleep(speed);
    }


    emit this->doRepaint();
    emit this->finished();
}

/*
    Getters and Setters
*/
int Simulator::getSpeed(){
    return speed;
}
void Simulator::setSpeed(int speed){
    this->speed = speed;
}
int Simulator::getIterationCount(){
    return iterationCount;
}
int Simulator::getCurrentCount(){
    return currentCount;
}
int Simulator::getTotalBirth(){
    return totalBirth;
}
int Simulator::getTotalDeath(){
    return totalDeath;
}
void Simulator::stopSimulation(){
    stop = true;
}
