#-------------------------------------------------
#
# Project created by QtCreator 2015-03-23T14:55:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Game_of_Life_Experimental_Tool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    simulator.cpp

HEADERS  += mainwindow.h \
    simulator.h

FORMS    += mainwindow.ui
CONFIG += c++11
