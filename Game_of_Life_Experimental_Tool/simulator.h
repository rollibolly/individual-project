#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <QMainWindow>
#include <QObject>
#include <QDebug>
#include <QThread>
#include <QKeyEvent>
#include <map>

using namespace std;

class Simulator : public QObject
{
    Q_OBJECT
public:
    typedef pair<int, int> Index;

    map<Index, int> *hashBoard;
    map<Index, int> *heatMap;
    map<Index, int> *updateList;

    bool heat;
    int dimX;
    int dimY;

    int iterationCount;
    int currentCount;
    int totalBirth;
    int totalDeath;

    bool stop;

    int speed;

    Simulator();
    ~Simulator();

    void insertLife(Index i);
    void removeLife(Index i);
    void uploadRandom(double precent);
    void incrementCell(Index i);
    void updateLists();
    void iterate();

    // Getters and Setters
    void setSpeed(int);
    int getSpeed();
    int getIterationCount();
    int getCurrentCount();
    int getTotalDeath();
    int getTotalBirth();
    void stopSimulation();

public slots:
    void process();
    void reloade();

signals:
    void drawDotAt(int x, int y);
    void clearDotAt(int x, int y);

    void updateCounters();
    void doRepaint();
    void finished();
};

#endif // SIMULATOR_H
