#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->init();

}
/*
    Initializing components and variables
*/
void MainWindow::init(){

    ui->sliderSpeed->setFocusPolicy(Qt::NoFocus);
    ui->sliderZoom->setFocusPolicy(Qt::NoFocus);
    ui->radioAntialiasign->setFocusPolicy(Qt::NoFocus);
    ui->radioHeat->setFocusPolicy(Qt::NoFocus);
    ui->buttonPause->setFocusPolicy(Qt::NoFocus);
    ui->buttonStart->setFocusPolicy(Qt::NoFocus);
    ui->buttonSearch->setFocusPolicy(Qt::NoFocus);
    ui->statusBackground->setFocusPolicy(Qt::NoFocus);
    ui->spinBoxInitialPopulation->setFocusPolicy(Qt::NoFocus);

    dotSize = 3;
    dotColor = Qt::white;
    background = Qt::black;

    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, Qt::black);
    ui->controlPanel->setAutoFillBackground(true);
    ui->controlPanel->setPalette(Pal);

    pixMap = new QPixmap(BOARD_WIDTH, BOARD_HEIGHT);
    pixMap->fill(background);
    painter = new QPainter(pixMap);
    painter->setRenderHint(QPainter::Antialiasing);

    brush = painter->brush();
    pen = painter->pen();

}

/*
    Methods for graphics:
*/
void MainWindow::drawDot(int x, int y){
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(dotColor);
    painter->setBrush(brush);
    painter->setPen(Qt::NoPen);
    painter->drawEllipse(CENTER_X + x * dotSize, CENTER_Y + y * dotSize, dotSize, dotSize);
    //this->repaint();
}

void MainWindow::clearDot(int x, int y){
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(background);
    pen.setColor(background);
    painter->setBrush(brush);
    painter->setPen(Qt::NoPen);
    painter->drawRect(CENTER_X + x * dotSize, CENTER_Y + y * dotSize, dotSize, dotSize);
    //this->repaint();
}

void MainWindow::clearScreen(){
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(background);
    pen.setColor(background);
    painter->setBrush(brush);
    painter->setPen(Qt::NoPen);
    painter->drawRect(0, 0, BOARD_WIDTH, BOARD_HEIGHT);
}

void MainWindow::paintEvent(QPaintEvent *){
    QPainter painter(this);

    painter.drawPixmap(0,0,800,600,*pixMap);
}

void MainWindow::updateCounters(){
    ui->lcdGeneration->display(simulator->getIterationCount());
    ui->lcdCurrent->display(simulator->getCurrentCount());
    ui->lcdBirth->display(simulator->getTotalBirth());
    ui->lcdDeath->display(simulator->getTotalDeath());
}

/*
    Handle KeyPress Events
*/
void MainWindow::keyPressEvent(QKeyEvent *event){

    bool hasToMove = true;
    switch (event->key()){
    case Qt::Key_Left:
        CENTER_X += 10;
        break;
    case Qt::Key_Right:
        CENTER_X -= 10;
        break;
    case Qt::Key_Down:
        CENTER_Y -= 10;
        break;
    case Qt::Key_Up:
        CENTER_Y += 10;
        break;
    case Qt::Key_Plus:
        dotSize++;
        break;
    case Qt::Key_Minus:
        if (dotSize > 1){
            dotSize--;
        }
        break;
    case Qt::Key_A:
        if (simulator->getSpeed() != 10){
            simulator->setSpeed(simulator->getSpeed() - 10);
        }
        break;
    case Qt::Key_Z:
        simulator->setSpeed(simulator->getSpeed() + 10);
        break;
    default:
        hasToMove = false;
        break;
    }
    if (hasToMove){
        this->clearScreen();
        simulator->reloade();
        repaint();
    }
}

MainWindow::~MainWindow()
{
    delete painter;
    delete pixMap;
    delete ui;
}

void MainWindow::startSimulation(){
    thread = new QThread;
    simulator = new Simulator();
    simulator->moveToThread(thread);
    simulator->uploadRandom(ui->spinBoxInitialPopulation->value());

    // Makeing connctions between Main window and Simulator
    connect(thread, SIGNAL(started()),simulator, SLOT(process()));
    connect(simulator, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(simulator, SIGNAL(finished()), simulator, SLOT(deleteLater()));

    connect(simulator, SIGNAL(drawDotAt(int,int)), this, SLOT(drawDot(int,int)));
    connect(simulator, SIGNAL(clearDotAt(int,int)), this, SLOT(clearDot(int,int)));
    connect(simulator, SIGNAL(doRepaint()), this, SLOT(repaint()));
    connect(simulator, SIGNAL(updateCounters()), this, SLOT(updateCounters()));
    thread->start();
}

void MainWindow::on_buttonStart_clicked()
{
    if (ui->buttonStart->text() == "Start"){
        ui->buttonStart->setText("Stop");
        startSimulation();

    } else if (ui->buttonStart->text() == "Stop"){
        simulator->stopSimulation();
        this->clearScreen();
        repaint();
        ui->buttonStart->setText("Start");
    }
}
