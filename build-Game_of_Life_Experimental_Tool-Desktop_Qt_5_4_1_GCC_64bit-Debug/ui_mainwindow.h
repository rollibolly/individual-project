/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *controlPanel;
    QVBoxLayout *verticalLayout;
    QPushButton *buttonSearch;
    QPushButton *buttonStart;
    QPushButton *buttonPause;
    QRadioButton *radioAntialiasign;
    QRadioButton *radioHeat;
    QDoubleSpinBox *spinBoxInitialPopulation;
    QGridLayout *gridLayout_2;
    QSlider *sliderZoom;
    QSlider *sliderSpeed;
    QLabel *label;
    QLabel *label_2;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLCDNumber *lcdCurrent;
    QLabel *label_6;
    QLCDNumber *lcdGeneration;
    QLabel *label_4;
    QLCDNumber *lcdBirth;
    QLabel *label_5;
    QLCDNumber *lcdDeath;
    QGraphicsView *statusBackground;
    QStatusBar *statusBar;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 600);
        MainWindow->setMinimumSize(QSize(800, 600));
        MainWindow->setMaximumSize(QSize(800, 600));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        controlPanel = new QWidget(centralWidget);
        controlPanel->setObjectName(QStringLiteral("controlPanel"));
        controlPanel->setGeometry(QRect(610, 60, 171, 391));
        controlPanel->setStyleSheet(QStringLiteral(""));
        verticalLayout = new QVBoxLayout(controlPanel);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        buttonSearch = new QPushButton(controlPanel);
        buttonSearch->setObjectName(QStringLiteral("buttonSearch"));

        verticalLayout->addWidget(buttonSearch);

        buttonStart = new QPushButton(controlPanel);
        buttonStart->setObjectName(QStringLiteral("buttonStart"));

        verticalLayout->addWidget(buttonStart);

        buttonPause = new QPushButton(controlPanel);
        buttonPause->setObjectName(QStringLiteral("buttonPause"));

        verticalLayout->addWidget(buttonPause);

        radioAntialiasign = new QRadioButton(controlPanel);
        radioAntialiasign->setObjectName(QStringLiteral("radioAntialiasign"));

        verticalLayout->addWidget(radioAntialiasign);

        radioHeat = new QRadioButton(controlPanel);
        radioHeat->setObjectName(QStringLiteral("radioHeat"));

        verticalLayout->addWidget(radioHeat);

        spinBoxInitialPopulation = new QDoubleSpinBox(controlPanel);
        spinBoxInitialPopulation->setObjectName(QStringLiteral("spinBoxInitialPopulation"));

        verticalLayout->addWidget(spinBoxInitialPopulation);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        sliderZoom = new QSlider(controlPanel);
        sliderZoom->setObjectName(QStringLiteral("sliderZoom"));
        sliderZoom->setMaximumSize(QSize(30, 150));
        sliderZoom->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(sliderZoom, 0, 1, 1, 1);

        sliderSpeed = new QSlider(controlPanel);
        sliderSpeed->setObjectName(QStringLiteral("sliderSpeed"));
        sliderSpeed->setMaximumSize(QSize(30, 150));
        sliderSpeed->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(sliderSpeed, 0, 0, 1, 1);

        label = new QLabel(controlPanel);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(50, 20));

        gridLayout_2->addWidget(label, 1, 1, 1, 1);

        label_2 = new QLabel(controlPanel);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(50, 20));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 781, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(horizontalLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(55, 16777215));

        horizontalLayout->addWidget(label_3);

        lcdCurrent = new QLCDNumber(horizontalLayoutWidget);
        lcdCurrent->setObjectName(QStringLiteral("lcdCurrent"));
        lcdCurrent->setDigitCount(10);
        lcdCurrent->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdCurrent);

        label_6 = new QLabel(horizontalLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMaximumSize(QSize(80, 16777215));

        horizontalLayout->addWidget(label_6);

        lcdGeneration = new QLCDNumber(horizontalLayoutWidget);
        lcdGeneration->setObjectName(QStringLiteral("lcdGeneration"));
        lcdGeneration->setDigitCount(10);
        lcdGeneration->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdGeneration);

        label_4 = new QLabel(horizontalLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMaximumSize(QSize(75, 16777215));

        horizontalLayout->addWidget(label_4);

        lcdBirth = new QLCDNumber(horizontalLayoutWidget);
        lcdBirth->setObjectName(QStringLiteral("lcdBirth"));
        lcdBirth->setDigitCount(10);
        lcdBirth->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdBirth);

        label_5 = new QLabel(horizontalLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMaximumSize(QSize(85, 16777215));

        horizontalLayout->addWidget(label_5);

        lcdDeath = new QLCDNumber(horizontalLayoutWidget);
        lcdDeath->setObjectName(QStringLiteral("lcdDeath"));
        lcdDeath->setDigitCount(10);
        lcdDeath->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdDeath);

        statusBackground = new QGraphicsView(centralWidget);
        statusBackground->setObjectName(QStringLiteral("statusBackground"));
        statusBackground->setGeometry(QRect(0, 0, 801, 31));
        MainWindow->setCentralWidget(centralWidget);
        statusBackground->raise();
        controlPanel->raise();
        horizontalLayoutWidget->raise();
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 800, 25));
        MainWindow->setMenuBar(menuBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        buttonSearch->setText(QApplication::translate("MainWindow", "Search", 0));
        buttonStart->setText(QApplication::translate("MainWindow", "Start", 0));
        buttonPause->setText(QApplication::translate("MainWindow", "Pause", 0));
        radioAntialiasign->setText(QApplication::translate("MainWindow", "Antialiasign", 0));
        radioHeat->setText(QApplication::translate("MainWindow", "Heat map", 0));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">Zoom</span></p></body></html>", 0));
        label_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">Speed</span></p></body></html>", 0));
        label_3->setText(QApplication::translate("MainWindow", "Current:", 0));
        label_6->setText(QApplication::translate("MainWindow", "Generation:", 0));
        label_4->setText(QApplication::translate("MainWindow", "Total birth:", 0));
        label_5->setText(QApplication::translate("MainWindow", "Total death:", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
